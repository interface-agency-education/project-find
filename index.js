if (process.env.NODE_ENV !== 'production') require('dotenv').config({ path: './../.env' });

var cors = require('cors')();
var projectSchema = require('./model');
var databaseConnection = require('@interface-agency/api-database-connection')('student');

module.exports.execute = function(request, response) {
	cors(request, response, function() {
		if (request.method === 'GET') {
			var Project = databaseConnection.model('Projects', projectSchema);
			Project.find({'clean': request.query.project_clean,
		}, function(err, project) {
		        if (err) return response.status(500).send(err);
		        return response.status(200).send(project);
		    });
		} else {
			return response.status(405);
		}
	});
}
